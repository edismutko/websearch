﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebSeach.Core.Entity;
using WebSeach.Core.Model;

namespace WebSeach.Mapping
{
    public class MappingProfile: Profile
    {
        public MappingProfile()
        {
            // Domain to Resource
            CreateMap<SearchRecord, SearchRecordDTO>();

            // Resource to Domain
            CreateMap<SearchRecordDTO, SearchRecord>();
        }
    }
}
