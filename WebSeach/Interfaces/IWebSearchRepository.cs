﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebSeach.Core.Entity;
using WebSeach.Core.Model;

namespace WebSeach.Interfaces
{
    public interface IWebSearchRepository
    {
        Task<IEnumerable<SearchRecord>> GetSearchResultAsync(string searchPhrase);
    }
}
