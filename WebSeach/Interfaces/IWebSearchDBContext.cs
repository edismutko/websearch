﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using WebSeach.Core.Entity;

namespace WebSeach.Interfaces
{
    public interface IWebSearchDBContext
    {
        Task SaveSearchTitlesAsync(IEnumerable<string> searchTitles);
    }
}
