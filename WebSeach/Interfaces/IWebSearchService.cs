﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebSeach.Core.Entity;

namespace WebSeach.Interfaces
{
    public interface IWebSearchService
    {
        Task<IEnumerable<SearchRecord>> SearchTop5Async(string searchPhrase);
        ESearchEngine SearchEngine { get; }
    }
}
