﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebSeach.Core.Entity
{
    public enum ESearchEngine
    {
        Google, Bing
    }

    public class SearchRecord
    {
        public ESearchEngine SeachEngine
        {
            get; set;
        }

        public string Title
        {
            get; set;
        }

        public DateTime EntredDate
        {
            get; set;
        }
    }
}
