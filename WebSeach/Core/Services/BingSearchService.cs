﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebSeach.Core.Entity;
using WebSeach.Interfaces;

namespace WebSeach.Core.Services
{
    public class BingSearchService : IWebSearchService
    {
        List<SearchRecord> _dummyData;

        public ESearchEngine SearchEngine => ESearchEngine.Bing;

        public BingSearchService()
        {
            PrepareDummyData();
        }

        private void PrepareDummyData()
        {
            _dummyData = new List<SearchRecord>()
            {
                new SearchRecord(){SeachEngine = SearchEngine, Title = "Test 1", EntredDate = DateTime.Now },
                new SearchRecord(){SeachEngine = SearchEngine, Title = "Test 2", EntredDate = DateTime.Now },
                new SearchRecord(){SeachEngine = SearchEngine, Title = "Test 3", EntredDate = DateTime.Now },
                new SearchRecord(){SeachEngine = SearchEngine, Title = "Test 4", EntredDate = DateTime.Now },
                new SearchRecord(){SeachEngine = SearchEngine, Title = "Test 5", EntredDate = DateTime.Now },
            };
        }
        public Task<IEnumerable<SearchRecord>> SearchTop5Async(string searchPhrase)
        {
            return Task<IEnumerable<SearchRecord>>.Run(() => {
                List<SearchRecord> result = new List<SearchRecord>();
                var searchResult = _dummyData.FindAll((record) => record.Title.Contains(searchPhrase));
                for (int i = 0; i < Math.Min(searchResult.Count, 5); ++i)
                {
                    result.Add(searchResult[i]);
                }
                return result.AsEnumerable();
            });
        }
    }
}
