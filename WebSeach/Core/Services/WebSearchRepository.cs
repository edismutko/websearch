﻿using AutoMapper;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebSeach.Core.Entity;
using WebSeach.Interfaces;

namespace WebSeach.Core.Services
{
    public class WebSearchRepository : IWebSearchRepository
    {
        IMemoryCache _cache;
        IWebSearchDBContext _dbContext;
        IEnumerable<IWebSearchService> _webSearchServices;

        public WebSearchRepository(IEnumerable<IWebSearchService> webSearchServices, IWebSearchDBContext dbContext, IMemoryCache cache)
        {
            _cache = cache;
            _webSearchServices = webSearchServices;
            _dbContext = dbContext;
        }

        async public Task<IEnumerable<SearchRecord>> GetSearchResultAsync(string searchPhrase)
        {
            var result = new List<SearchRecord>();
            foreach(var service in _webSearchServices)
            {
                result.AddRange(await service.SearchTop5Async(searchPhrase));
            }

            await _dbContext.SaveSearchTitlesAsync(result.Select((record) => record.Title));
            
            return result;
        }
    }
}
