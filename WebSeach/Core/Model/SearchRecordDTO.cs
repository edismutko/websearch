﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebSeach.Core.Entity;

namespace WebSeach.Core.Model
{
    public class SearchRecordDTO
    {
        public ESearchEngine SeachEngine
        {
            get; set;
        }

        public string Title
        {
            get; set;
        }

        public DateTime EntredDate
        {
            get; set;
        }
    }
}
