﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebSeach.Core.Model;
using WebSeach.Interfaces;

namespace WebSeach.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class WebSearchController : ControllerBase
    {
        private IWebSearchRepository _searchRepository;
        private IMapper _mapper;

        public WebSearchController(IWebSearchRepository searchRepository, IMapper mapper)
        {
            _searchRepository = searchRepository;
            _mapper = mapper;
        }

        [HttpGet("{searchPhrase}")]
        public async Task<ActionResult<IEnumerable<SearchRecordDTO>>> Get(string searchPhrase)
        {
            return Ok((await _searchRepository.GetSearchResultAsync(searchPhrase)).Resolve(_mapper));
        }
    }
}