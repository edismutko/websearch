﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebSeach.Core.Entity;
using WebSeach.Core.Model;

namespace WebSeach
{
    public static class Extensions
    {
        public static IEnumerable<SearchRecordDTO> Resolve(this IEnumerable<SearchRecord> searchResult, IMapper mapper)
        {
            return searchResult.Select<SearchRecord, SearchRecordDTO>((record) => mapper.Map<SearchRecord, SearchRecordDTO>(record));
        }
    }
}
